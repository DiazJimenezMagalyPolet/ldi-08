/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bites;

/**
 *
 * @author hergusalap
 */
public class OpLogicas {

    Bites resultado=new Bites();
    Bites r=new Bites();

    public OpLogicas() {
    }

    ;

    public void opAnd(Bites a, Bites b) {
        for (int i=resultado.bite.length-1;i>=0; i--) {
            if (a.bite[i] == 1 && b.bite[i] == 1) {
                resultado.bite[i] = 1;
          //      System.out.print(resultado.bite[i]);
            } else {
                resultado.bite[i] = 0;
        //        System.out.print(resultado.bite[i]);
            }
        }
         r.imprimirD(resultado);
    }

    public void opOr(Bites a, Bites b) {
        for (int i=resultado.bite.length-1;i>=0; i--) {
            if (a.bite[i] == 0 && b.bite[i] == 0) {
                resultado.bite[i] = 0;
                //System.out.print(resultado.bite[i]);
            } else {
                resultado.bite[i] = 1;
                //System.out.print(resultado.bite[i]);
            }
        }
       r.imprimirD(resultado);
    }

    public void opNot(Bites a) {
        for (int i=resultado.bite.length-1;i>=0; i--) {
            if (a.bite[i] == 0) {
                resultado.bite[i] = 1;
               // System.out.print(resultado.bite[i]);
            } else {
                resultado.bite[i] = 0;
                //System.out.print(resultado.bite[i]);
            }
        }
        r.imprimirD(resultado);
    }

    public static void main(String[] a) {
        OpLogicas op = new OpLogicas();
        Bites res = new Bites();
        
        Bites b1 = new Bites(1, 1, 0, 1, 0, 1, 1, 0);
        Bites b2 = new Bites(0, 1, 0, 1, 1, 0, 1, 0);

        System.out.print("\t\tByte 1    ");
        res.imprimirD(b1);
        System.out.print("\n\t\tByte 2    ");
        res.imprimirD(b2);

        System.out.println("\n\nResultado de operacion AND sobre 1 y 2");
        op.opAnd(b1, b2);
        System.out.println("\n\nResultado de operacion OR sobre 1 y 2");
        op.opOr(b1, b2);
        System.out.println("\n\nResultado de operacion NOT sobre 1");
        op.opNot(b1);
     
    }

}
