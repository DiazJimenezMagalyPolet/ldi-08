/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bites;

/**
 *
 * @author hergusalap
 */
public class Bites {

    int bite[] = new int[8];
    int temp[] = new int[8];

    public Bites() {

    }

    public Bites(int a, int b, int c, int d, int e, int f, int g, int h) {
        bite[0] = a;
        bite[1] = b;
        bite[2] = c;
        bite[3] = d;
        bite[4] = e;
        bite[5] = f;
        bite[6] = g;
        bite[7] = h;
    }

    public Bites sumaBites(Bites a, Bites b) {
        temp = new int[8];
        Bites tempo = new Bites();
        int acarreo = 0;
        int inicial = 0;
        for (int i = (a.bite.length - 1); i > -1; i--) {

            if ((b.bite[i] == 0 && a.bite[i] == 1) || (b.bite[i] == 1 && a.bite[i] == 0)) {
                if (acarreo > 0) {
                    acarreo = acarreo;
                    temp[i] = 0;
                    tempo.bite[i] = 0;
                } else {
                    acarreo = acarreo;
                    temp[i] = 1;
                    tempo.bite[i] = 1;
                }
            }
            if (b.bite[i] == 0 && a.bite[i] == 0) {
                if (acarreo > 0) {
                    acarreo--;
                    temp[i] = 1;
                    tempo.bite[i] = 1;
                } else {
                    temp[i] = 0;
                    tempo.bite[i] = 0;
                }
            }

            if ((b.bite[i] == 1 && a.bite[i] == 1)) {
                //acarreo++;
                if (acarreo > 0) {
                    temp[i] = 1;
                    tempo.bite[i] = 1;
                    acarreo = acarreo;
                } else {
                    temp[i] = 0;
                    tempo.bite[i] = 0;
                    acarreo++;
                }
            }
        }
        return tempo;
    }

    public void imprimirD(Bites a) {
        for (int i = 0; i < a.bite.length-1; i++) {
            System.out.print(a.bite[i] + " ");
        }
    }

    
    public static void main(String arg[]) {
        Bites sp = new Bites();

        Bites a = new Bites(1, 1, 1, 1, 0, 1, 0, 1);
        Bites c = new Bites(0, 0, 0, 0, 0, 0, 0, 1);
        sp.imprimirD(sp.sumaBites(a, c));
    }

}
